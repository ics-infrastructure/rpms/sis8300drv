# sis8300drv

CentOS RPM for [sis8300drv](https://gitlab.esss.lu.se/epics-modules/rf/sis8300drv): driver for Struck sis8300 DAQ card 

This RPM includes:

- sis8300drv shared object library
- sis8300drv tools

To build a new version of the RPM:

- update the version in the spec file
- push your changes to check that the RPM can be built by gitlab-ci
- tag the repository and push the tag for the RPM to be uploaded to artifactory rpm-ics repo
