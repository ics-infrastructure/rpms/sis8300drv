%define module_name sis8300drv
%define version 4.10.2

Name:           %{module_name}
Version:        %{version}
Release:        0
Summary:        Shared library and tools for the sis8300 driver
License:        GPL
URL:            https://gitlab.esss.lu.se/epics-modules/rf/sis8300drv
Source:         https://gitlab.esss.lu.se/epics-modules/rf/sis8300drv/-/archive/%{version}/sis8300drv-%{version}.tar.gz
BuildRequires:  autoconf libtool gcc gcc-c++
Buildroot:      %{_tmppath}/%{name}-%{version}-root

%description
Library and tools sis8300 DAQ card

%prep
%setup -qn sis8300drv-%{version}

%build
cd src/main/c/lib
make
cd ../tools
make

%clean
rm -fr $RPM_BUILD_ROOT

%install
install -d %{buildroot}/%{_libdir}
install -m 0755 src/main/c/lib/libsis8300drv.so %{buildroot}/%{_libdir}

install -d %{buildroot}/%{_bindir}
cp src/main/c/tools/acquisition/sis8300drv_acq   %{buildroot}/%{_bindir}/sis8300drv_acq
cp src/main/c/tools/flash/sis8300drv_flashfw     %{buildroot}/%{_bindir}/sis8300drv_flashfw
cp src/main/c/tools/fwver/sis8300drv_fwver       %{buildroot}/%{_bindir}/sis8300drv_fwver
cp src/main/c/tools/i2c_rtm/sis8300drv_i2c_rtm   %{buildroot}/%{_bindir}/sis8300drv_i2c_rtm
cp src/main/c/tools/i2c_temp/sis8300drv_i2c_temp %{buildroot}/%{_bindir}/sis8300drv_i2c_temp
cp src/main/c/tools/irq/sis8300drv_irq           %{buildroot}/%{_bindir}/sis8300drv_irq
cp src/main/c/tools/memory/sis8300drv_mem        %{buildroot}/%{_bindir}/sis8300drv_mem
cp src/main/c/tools/mmap/sis8300drv_mmap         %{buildroot}/%{_bindir}/sis8300drv_mmap
cp src/main/c/tools/output/sis8300drv_out        %{buildroot}/%{_bindir}/sis8300drv_out
cp src/main/c/tools/performance/sis8300drv_perf  %{buildroot}/%{_bindir}/sis8300drv_perf
cp src/main/c/tools/register/sis8300drv_reg      %{buildroot}/%{_bindir}/sis8300drv_reg
cp src/main/c/tools/speed/sis8300drv_speed       %{buildroot}/%{_bindir}/sis8300drv_speed

%files
%license LICENSE
%{_libdir}/libsis8300drv.so
%{_bindir}/sis8300drv_acq
%{_bindir}/sis8300drv_flashfw
%{_bindir}/sis8300drv_fwver
%{_bindir}/sis8300drv_i2c_rtm
%{_bindir}/sis8300drv_i2c_temp
%{_bindir}/sis8300drv_irq
%{_bindir}/sis8300drv_mem
%{_bindir}/sis8300drv_mmap
%{_bindir}/sis8300drv_out
%{_bindir}/sis8300drv_perf
%{_bindir}/sis8300drv_reg
%{_bindir}/sis8300drv_speed
